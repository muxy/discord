package discord

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// request fires of a low-level http request to Discord and returns the resulting
// JSON blob.
func (c *DiscordClient) request(path string) ([]byte, error) {
	url := fmt.Sprintf("%s/%s", "https://discordapp.com/api", path)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	if len(c.authKey) > 0 {
		req.Header.Add("Authorization", c.authKey)
	}

	res, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, err
	}

	defer res.Body.Close()

	contents, err := ioutil.ReadAll(res.Body)
	return contents, err
}

// decodeJSON is a convenience function to decode the provided JSON blob into
// the `out` object.
func decodeJSON(in []byte, out interface{}) error {
	if len(in) == 0 {
		return nil
	}

	if err := json.Unmarshal(in, out); err != nil {
		log.Printf("Failed decoding JSON: %s", string(in))
		return err
	}

	return nil
}
