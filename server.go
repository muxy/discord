package discord

import "fmt"

// Server holds the data returned from the /users/:id/guilds Discord endpoint.
type Server struct {
	ID          string `json:"id"`
	Owner       bool   `json:"owner"`
	OwnerID     string `json:"owner_id"`
	Name        string `json:"name"`
	Permissions int64  `json:"permissions"`
	Icon        string `json:"icon"`
}

// Channel holds the data returned from the /guilds/:guild_id/channels endpoint.
type Channel struct {
	ID      string `json:"id"`
	GuildID string `json:"guild_id"`

	Name  string `json:"name"`
	Type  int    `json:"type"`
	Topic string `json:"topic"`

	IsPrivate bool  `json:"is_private"`
	Position  int64 `json:"position"`

	Bitrate int64 `json:"bitrate"`

	LastMessageID string `json:"last_message_id"`

	PermissionOverwrites []PermissionOverwrite `json:"permission_overwrites"`
}

// PermissionOverwrite holds structure data for overwriting a server's default permissions.
type PermissionOverwrite struct {
	ID    string `json:"id"`
	Type  string `json:"type"`
	Allow int64  `json:"allow"`
	Deny  int64  `json:"deny"`
}

// GetCurrentUserServers returns a list of Server objects populated with the reponse
// from /api/users/@me/guilds
func (c *DiscordClient) GetCurrentUserServers() (servers []Server, err error) {
	json, err := c.request("users/@me/guilds")
	if err == nil {
		err = decodeJSON(json, &servers)
	}

	return
}

// GetServerForID returns server info from the given id.
func (c *DiscordClient) GetServerForID(serverID string) (server Server, err error) {
	json, err := c.request(fmt.Sprintf("guilds/%s", serverID))
	if err == nil {
		err = decodeJSON(json, &server)
	}

	return
}

// GetChannelListForServer returns a list of channels for the given server id.
func (c *DiscordClient) GetChannelListForServer(serverID string) (channels []Channel, err error) {
	json, err := c.request(fmt.Sprintf("guilds/%s/channels", serverID))
	if err == nil {
		err = decodeJSON(json, &channels)
	}

	return
}
