package discord

import "net/http"

// DiscordClient is the main interface to send and request information from Discord.
type DiscordClient struct {
	client  *http.Client
	authKey string
}

// NewClient wraps the provided http.Client with ease-of-use Discord requests.
func NewClient(httpClient *http.Client, authKey string) *DiscordClient {
	return &DiscordClient{
		client:  httpClient,
		authKey: authKey,
	}
}
