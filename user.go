package discord

// User holds the data returned from the /users Discord endpoint.
type User struct {
	ID            string `json:"id"`
	Discriminator string `json:"discriminator"`
	Avatar        string `json:"avatar"`
	Username      string `json:"username"`
}

// GetCurrentUser returns a User object populated with the reponse from
// /api/users/@me
func (c *DiscordClient) GetCurrentUser() (User, error) {
	var user User

	json, err := c.request("users/@me")
	if err == nil {
		err = decodeJSON(json, &user)
	}

	return user, err
}
